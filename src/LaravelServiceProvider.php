<?php

namespace Morilog\LaravelPaymand;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;
use Morilog\Paymand\ClientFactory;
use Morilog\Paymand\Config\Config;
use Morilog\Paymand\Config\ConfigBuilder;
use Morilog\Paymand\GatewayFactory;
use Morilog\Paymand\Paymand;

final class LaravelServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            'paymand.php' => __DIR__ . '/config.php',
        ], 'config');
    }

    public function register()
    {
        $this->app->singleton(Config::class, function (Container $app) {
            $laraConfig = $app->make('config');
            $builder = ConfigBuilder::createBuilder();
            if ((bool)$laraConfig->get('paymand.http_proxy.enabled', false)) {
                $builder->enableProxy()
                    ->setProxyHost(
                        $laraConfig->get('paymand.http_proxy.host'),
                        (bool)$laraConfig->get('paymand.http_proxy.is_https', false)
                    )
                    ->setProxyPort((int)$laraConfig->get('paymand.http_proxy.port'))
                    ->setProxyAuth(
                        $laraConfig->get('paymand.http_proxy.username'),
                        $laraConfig->get('paymand.http_proxy.password')
                    );
            }

            $builder->setDefaultGateway($laraConfig->get('paymand.default'));

            if ((bool)$laraConfig->get('paymand.ha_mode.enabled', false)) {
                $builder->enableHighAvailable((int)$laraConfig->get('paymand.ha_mode.max_tries'));
            }

            foreach ($laraConfig->get('paymand.gateways', []) as $name => $config) {
                if (array_get($config, 'enabled', false) === false) {
                    continue;
                }

                $builder->withGateway($name, $config);
            }

            return $builder->build();
        });

        $this->app->singleton(Paymand::class, function (Container $app) {
            /** @var Config $config */
            $config = $app->make(Config::class);
            $paymand = new Paymand($config);
            $clientFactory = $app->make(ClientFactory::class);
            $gatewayFactory = new GatewayFactory($config, $clientFactory);

            // Register default gateways
            foreach ($config->getGatewaysConfig() as $gatewayConfig) {
                try {
                    $paymand->addGateway($gatewayFactory->make($gatewayConfig->getName()), (int)$gatewayConfig->get('priority', 1));
                } catch (\InvalidArgumentException $e) {
                    continue;
                }
            }

            return $paymand;
        });
    }

    public function provides()
    {
        return [Paymand::class];
    }
}
