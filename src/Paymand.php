<?php

namespace Morilog\LaravelPaymand;

use Illuminate\Support\Facades\Facade;
use Morilog\Paymand\Paymand as MainPaymand;

final class Paymand extends Facade
{
    protected static function getFacadeAccessor()
    {
        return MainPaymand::class;
    }
}
