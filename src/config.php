<?php

return [
    'default' => null,
    'ha_mode' => [
        'enabled' => true,
        'max_tries' => 3
    ],
    'http_proxy' => [
        'enabled' => false,
        'host' => 'localhost',
        'port' => 1080,
        'username' => null,
        'password' => null,
        'is_https' => false,
    ],
    'gateways' => [
        'zarinpal' => [
            'enabled' => true,
            'merchant_id' => '',
            'priority' => 1,
        ],
        'pay_ir' => [
            'enabled' => true,
            'api_key' => '',
            'priority' => 2,
        ],
        'sadad' => [
            'enabled' => true,
            'merchant_id' => '',
            'terminal_id' => '',
            'terminal_key' => '',
        ]
    ]
];